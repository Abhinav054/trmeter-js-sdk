"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CustomerplanService = void 0;
var _axios = _interopRequireDefault(require("axios"));
var _env = require("../env");
var _Customerplan = require("../models/Customerplan");
var _utils = require("./utils");
var _lodash = require("lodash");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
class CustomerplanService {
  constructor(apikey) {
    this.api_key = apikey;
    this.apiHeaders = {
      "Authorization": "Bearer " + this.api_key
    };
  }
  async getPlan(id) {
    let res = await _axios.default.get(_env.apiUrl + "plans/" + id, {
      headers: this.apiHeaders
    });
    return res.data;
  }
  async createNewCustomerPlan(customer, plan) {
    // get plan
    let planobj;
    try {
      planobj = await this.getPlan(plan);
    } catch (err) {
      throw new Error(err.message);
    }

    // create a new customer plan object
    let customerplan = new _Customerplan.Customerplan(this.api_key);
    customerplan.customer = customer;
    customerplan.plan = plan;
    customerplan.custom_data = {};
    customerplan.start_date = (0, _utils.getFormattedDate)(new Date());
    customerplan.end_date = "";
    customerplan.ongoing = true;
    customerplan.discounts = {};
    customerplan.extra_charges = [];
    if (planobj.plan_type == "subscription") {
      customerplan.allowed_licenses = "1";
    }
    if (planobj.plan_type == "usage_based") {
      customerplan.allowed_licenses = "all";
    }
    customerplan.auto_subaccounts = true;
    customerplan.trial_config = {
      metrictrials: [],
      time_trial: {},
      trial_span: "once-per-customer"
    };
    if (!(0, _lodash.isEmpty)(planobj.trial_config)) {
      if (planobj.trial_config.metrictrials != undefined && planobj.trial_config.metrictrials.length != 0) {
        customerplan.trial_config.metrictrials = planobj.trial_config.metrictrials;
      }
      if (!(0, _lodash.isEmpty)(planobj.trial_config.time_trial)) {
        customerplan.trial_config.time_trial = planobj.trial_config.time_trial;
      }
    }
    customerplan.auto_charge = false;
    customerplan.payment_mode = "offline";
    customerplan.status = "in-use";
    customerplan.plan_currency = planobj.plan_pricing.currency;
    return customerplan;
  }
  async getCustomerplanById(id) {
    let res = await _axios.default.get("/customer-plans/" + id, {
      headers: this.apiHeaders
    });
    return res.data;
  }
}
exports.CustomerplanService = CustomerplanService;