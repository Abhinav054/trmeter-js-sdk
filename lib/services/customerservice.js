"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CustomerService = void 0;
var _env = require("../env");
var _axios = _interopRequireDefault(require("axios"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
class CustomerService {
  constructor(apitoken) {
    this.apitoken = apitoken;
    this.apiheaders = {
      "Authorization": "Bearer " + this.apitoken
    };
  }
  async getcustomer() {}
  async getActiveCustomerPlan() {
    let res = await _axios.default.get(_env.apiUrl, {
      headers: this.apiheaders
    });
    return res.data;
  }
  async createCustomer(customer) {
    try {
      let res = await _axios.default.post(_env.apiUrl + "customers", customer, {
        headers: this.apiheaders
      });
      return res.data;
    } catch (err) {
      throw new Error(err.response.data[0]);
    }
  }
  async checkCustomerExist(email) {
    let res = await _axios.default.get(_env.apiUrl + "customers/check/" + email, {
      headers: this.apiheaders
    });
    return res.data;
  }
  gettoken() {
    return this.apitoken;
  }
}
exports.CustomerService = CustomerService;