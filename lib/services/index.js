"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TrMeter = void 0;
var _customerplanService = require("./customerplanService");
var _customerservice = require("./customerservice");
class TrMeter {
  constructor(apitoken) {
    this.apitoken = apitoken;
    this.customerService = new _customerservice.CustomerService(this.apitoken);
    this.customerplanService = new _customerplanService.CustomerplanService(this.apitoken);
  }
  getToken() {
    return this.apitoken;
  }
}
exports.TrMeter = TrMeter;