"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFormattedDate = getFormattedDate;
function getFormattedDate(date) {
  let current_date = date;
  let current_date_string = current_date.getFullYear().toString();
  if (current_date.getMonth() + 1 < 10) {
    current_date_string = current_date_string + "-0" + (current_date.getMonth() + 1).toString();
  } else {
    current_date_string = current_date_string + "-" + (current_date.getMonth() + 1).toString();
  }
  if (current_date.getDate() < 10) {
    current_date_string = current_date_string + "-0" + current_date.getDate().toString();
  } else {
    current_date_string = current_date_string + "-" + current_date.getDate();
  }
  return current_date_string;
}
;