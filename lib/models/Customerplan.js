"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Customerplan = void 0;
var _axios = _interopRequireDefault(require("axios"));
var _env = require("../env");
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
class Customerplan {
  // API KEY

  apiHeaders = {};
  constructor(apikey) {
    this.api_key = apikey;
    this.apiHeaders = {
      "Authorization": "Bearer " + this.api_key
    };
  }
  async save() {
    let customerplan = {
      customer: this.customer,
      plan: this.plan,
      custom_data: this.custom_data,
      start_date: this.start_date,
      end_date: this.end_date,
      ongoing: this.ongoing,
      discounts: this.discounts,
      extra_charges: this.extra_charges,
      allowed_licenses: this.allowed_licenses,
      auto_subaccounts: this.auto_subaccounts,
      license_keys: this.license_keys,
      status: this.status,
      trial_config: this.trial_config,
      trial_usage_exists: this.trial_usage_exists,
      auto_charge: this.auto_charge,
      payment_mode: this.payment_mode,
      plan_currency: this.plan_currency
    };
    try {
      let res = await _axios.default.post(_env.apiUrl + "customer-plans/", customerplan, {
        headers: this.apiHeaders
      });
      return res.data;
    } catch (err) {
      console.log(err.response.data);
      // throw(new Error(err.response.data[0]));
    }
  }

  // getById(id: string) : Customerplan{
  //     let customerPlan = new Customerplan();
  //     return customerPlan;
  // } 
}
exports.Customerplan = Customerplan;