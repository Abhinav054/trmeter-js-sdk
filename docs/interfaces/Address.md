[trmeter-js-sdk](../README.md) / [Exports](../modules.md) / Address

# Interface: Address

## Table of contents

### Properties

- [addressLine1](Address.md#addressline1)
- [addressLine2](Address.md#addressline2)
- [city](Address.md#city)
- [country](Address.md#country)
- [landmark](Address.md#landmark)
- [state](Address.md#state)

## Properties

### addressLine1

• **addressLine1**: `string`

#### Defined in

models/Address.ts:7

___

### addressLine2

• **addressLine2**: `string`

#### Defined in

models/Address.ts:8

___

### city

• **city**: `string`

#### Defined in

models/Address.ts:5

___

### country

• **country**: `string`

#### Defined in

models/Address.ts:6

___

### landmark

• **landmark**: `string`

#### Defined in

models/Address.ts:9

___

### state

• **state**: `string`

#### Defined in

models/Address.ts:4
