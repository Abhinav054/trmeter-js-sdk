import {describe, expect, test} from '@jest/globals';

import {CustomerService} from '../src/services/customerservice';
import {CustomerplanService} from "../src/services/customerplanService";
const trmeter = require('../lib/index');
// import {trmeter} from '../index';


/** this code is called once before any test is called */



let apitoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzN2Y1M2UzZTQwNjg5MzI1Nzg2MmE3ZCIsImlhdCI6MTY2OTYyODgxMywiZXhwIjo0ODI1Mzg4ODEzfQ.DCNM4JyqGXxDObf9UqsfYDTd1eRtW6LHRHxZoxPAXXQ";

let truemeter = new trmeter.TrMeter(apitoken)

test('create customer test', async() => {
    // expect(inchesOfRain()).toBe(0);
    let customerservice = new CustomerService(apitoken);

    let customer  = {
        "name": "test1",
        "customer_type": "company",
        "company_name": "Test company",
        "email": "test@abc.com",
        "contact_number": "918882800976",
        "customer_address": {
            "state": "uttar pradesh",
            "country": "india",
            "city": "noida",
            "addressLine1": "Test 2134",
            "addressLine2": "riutoir sdijfe",
            "landmark": "8ur43t4"
        },
        "gstin_number": "ttttrtttttrttrtrt"
    };
    try{
        let res = await customerservice.createCustomer(customer);
        console.log(res);
    }catch(err){
        console.log(err.response.data);
    }
    console.log("test it yo");
});


test("check client exist", async()=>{

    let customerservice = new CustomerService(apitoken);
    try{
        let res = await  truemeter.customerService.checkCustomerExist("test@abc.com")
        console.log(res);
    }catch(err){
        console.log(err.response.data);
    }
})


test("create customer plan", async()=>{

    let customerservice = new CustomerService(apitoken);

    let customer  = {
        "name": "test1",
        "customer_type": "company",
        "company_name": "Test company",
        "email": "test_plan1@abc.com",
        "contact_number": "918882870976",
        "customer_address": {
            "state": "uttar pradesh",
            "country": "india",
            "city": "noida",
            "addressLine1": "Test 2134",
            "addressLine2": "riutoir sdijfe",
            "landmark": "8ur43t4"
        },
        "gstin_number": "ttttrtttttrttrtrt"
    };
    let customerres;
    try{
        customerres = await customerservice.createCustomer(customer);
        console.log(customerres);
    }catch(err){
        console.log(err.response.data);
    }

    let customerplanservice = new CustomerplanService(apitoken);

    let customerplan = await customerplanservice.createNewCustomerPlan(customerres.id,"637f684a5c041737cb339ae8");
    console.log(customerplan);
    await customerplan.save()

})


// it("test", async()=>{

//     console.log("this is the test");


// })
  
