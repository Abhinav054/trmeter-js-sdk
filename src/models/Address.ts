
export interface Address {

        state: string,
        city: string,
        country: string,
        addressLine1: string,
        addressLine2: string,
        pincode: string,
        landmark: string
    
}