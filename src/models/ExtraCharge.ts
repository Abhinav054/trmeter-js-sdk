export interface ExtraCharge {
    name: string,
    value: number,
    inclusiveTaxes: boolean,
    taxslab: string,
    type: "one-time-per-customer"|"one-time-per-subaccount"|"recurring-per-customer"|"recurring-per-subaccount",
    model_type: "template"|"custom",
    currency: string,
    availed: boolean
}