export interface Poc {
    name: string,
    email: string,
    department: string,
    contactNumber: string
}