
export interface PlanpricingSubscription{
    value: number,
    currency: string
}