export interface FixedDiscounts {
    type: "fixed-one-time" | "fixed-recurring",
    availed: boolean,
    discountvaltype: "flat" | "percentage",
    discountval: number,
    adddiscextracharges: boolean
}


export interface MetricDiscounts {

    type: "metric-one-time" | "metric-recurring",
    discount_metrics: MetricDiscount[]

}

interface MetricDiscount{
    metric: string,
    unit: string,
    type: "usage" | "percentage",
    value: number
}