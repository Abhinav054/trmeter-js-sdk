import { PlanpricingSubscription } from "./PlanpricingSubscription"
import { PlanpricingUsagebased } from "./PlanpricingUsagebased"
import { FixedDiscounts,MetricDiscounts } from "./Discounts"
import { ExtraCharge } from "./ExtraCharge";
import axios from "axios";
import { apiUrl } from "../env";

export class Customerplan{
    
    id: string;
    customer: string;
    plan: string;
    custom_data: PlanpricingSubscription | PlanpricingUsagebased | {};
    start_date: string;
    end_date: string;
    mupdatedAt: Date;
    cycle_start_date: Date;
    cycle_end_date: Date;
    ongoing: boolean;
    discounts: FixedDiscounts | MetricDiscounts | {};
    bill: number;
    consumption: any;
    consumption_limits: any;
    customer_settings: any;
    extra_charges: ExtraCharge[] | [];
    allowed_licenses: string;
    auto_subaccounts: boolean;
    license_keys: any;
    active_licenses: number;
    status: "in-use" | "in-active" | "in-trial" | "payment-pending";
    trial_config: any;
    trial_usage_exists: boolean;
    auto_charge: boolean;
    payment_mode: "offline"|"online";
    totalrev: number;
    plan_currency: string;
    totalrec: number

    // API KEY
    api_key: string;
    apiHeaders = {};

    constructor(apikey: string){
        this.api_key = apikey;
        this.apiHeaders = {
            "Authorization": "Bearer "+this.api_key
        }
    }

    
    async save():Promise<any>{

        let customerplan={
            customer: this.customer,
            plan:this.plan,
            custom_data:this.custom_data,
            start_date:this.start_date,
            end_date:this.end_date,
            ongoing: this.ongoing,
            discounts: this.discounts,
            extra_charges: this.extra_charges,
            allowed_licenses: this.allowed_licenses,
            auto_subaccounts: this.auto_subaccounts,
            license_keys: this.license_keys,
            status: this.status,
            trial_config: this.trial_config,
            trial_usage_exists: this.trial_usage_exists,
            auto_charge: this.auto_charge,
            payment_mode: this.payment_mode,
            plan_currency: this.plan_currency
        }
        try{
            let res = await axios.post(apiUrl+"customer-plans/",customerplan,{headers: this.apiHeaders});
            return res.data;
        }catch(err){
            console.log(err.response.data);
            // throw(new Error(err.response.data[0]));

        }
        
    }


    // getById(id: string) : Customerplan{
    //     let customerPlan = new Customerplan();
    //     return customerPlan;
    // } 
}