import { Address } from "./Address";
import { Poc } from "./Poc";

export interface Customer {
    name: string;
    customer_type: string;
    customer_id: string;
    company_name: string;
    email: string;
    contact_number: string;
    customer_address: Address;
    gstin_number: string;
    status: boolean;
    poc: Poc[];
    sub_accounts: Subaccount[];
    id: number;

}

interface Subaccount {

    email: string,
    apiKey: string

}




