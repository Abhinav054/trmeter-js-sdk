import { PlanpricingSubscription } from "./PlanpricingSubscription";
import { PlanpricingUsagebased } from "./PlanpricingUsagebased";

export interface Plan {

    name: string,
    product: string,
    plan_type: "subscription" | "usage_based",
    plan_pricing: PlanpricingSubscription|PlanpricingUsagebased,
    plan_duration_type: string,
    plan_duration: string,
    payment_method: string,
    plan_features: any,
    desc: string,
    trial_config: trialConfig | {},
    disabled: boolean,
    inclusivetaxes: boolean,
    extra_features: any,
    plan_limits: any
    


}

interface trialConfig{

    time_trial: timeTrial | {};
    metrictrials: metricTrial[] | [];

}

interface timeTrial{
    unit: string;
    qnt: string;
}


interface metricTrial{
    name: string;
    unit: string;
    value: number
}