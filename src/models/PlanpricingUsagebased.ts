
export interface PlanpricingUsagebased{
    billable_metrics: MetricPrice[],
    currency: string
}


interface MetricPrice {
    metric_type: string,
    name: string,
    aggregator: string,
    unit: string,
    pricing_type: string,
    value: number
} 