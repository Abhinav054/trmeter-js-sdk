import { Customer } from '../models/Customer';
import { apiUrl } from '../env';
import axios from 'axios';


export class CustomerService{

    apitoken: string;

    apiheaders: any;

    constructor(apitoken: string){
        this.apitoken=apitoken;
        this.apiheaders = {
            "Authorization": "Bearer "+this.apitoken
        };
    }

    async getcustomer(){

    }

    async getActiveCustomerPlan(){
        let res = await axios.get(apiUrl,{headers:this.apiheaders});
        return res.data;
    }

    async createCustomer(customer: Customer){
        try{
            let res = await axios.post(apiUrl+"customers",customer,{headers:this.apiheaders});
            return res.data;
        }catch(err){
            throw new Error(err.response.data[0]);
        }
        
    }

    async checkCustomerExist(email: string){
        let res = await axios.get(apiUrl+"customers/check/"+email,{headers: this.apiheaders});
        return res.data;
    }

    gettoken(){
        return this.apitoken;
    }

}