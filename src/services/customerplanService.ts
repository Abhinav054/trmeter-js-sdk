import axios from "axios";
import { apiUrl } from "../env";
import { Customerplan } from "../models/Customerplan";
import { Plan } from "../models/Plans";
import {getFormattedDate} from './utils';
import {isEmpty} from 'lodash';


export class CustomerplanService{

    api_key: string;

    apiHeaders: any;

    constructor(apikey: string){
        this.api_key = apikey;
        this.apiHeaders = {
            "Authorization": "Bearer "+this.api_key
        };
    }


    async getPlan(id: string):Promise<Plan> {

        let res = await axios.get(apiUrl+"plans/"+id,{headers: this.apiHeaders});
        return res.data;

    }

    async createNewCustomerPlan(customer:string,plan:string): Promise<Customerplan>{
        
        // get plan
        let planobj:Plan;
        try{
            planobj = await this.getPlan(plan);
        }catch(err){
            throw new Error(err.message);
        }
        
        // create a new customer plan object
        let customerplan = new Customerplan(this.api_key);
        customerplan.customer = customer;
        customerplan.plan = plan;
        customerplan.custom_data = {};
        customerplan.start_date = getFormattedDate(new Date());
        customerplan.end_date = "";
        customerplan.ongoing = true;
        customerplan.discounts = {};
        customerplan.extra_charges = [];
        
        if(planobj.plan_type=="subscription"){
            customerplan.allowed_licenses = "1";
        }

        if(planobj.plan_type=="usage_based"){
            customerplan.allowed_licenses = "all";
        }

        customerplan.auto_subaccounts = true;

        customerplan.trial_config = {
            metrictrials: [],
            time_trial: {},
            trial_span: "once-per-customer"
        };

        if(!isEmpty(planobj.trial_config)){
            if(planobj.trial_config.metrictrials!=undefined&&planobj.trial_config.metrictrials.length!=0){
                customerplan.trial_config.metrictrials = planobj.trial_config.metrictrials;
            }

            if(!isEmpty(planobj.trial_config.time_trial)){
                customerplan.trial_config.time_trial = planobj.trial_config.time_trial;
            }
        }

        customerplan.auto_charge = false;
        customerplan.payment_mode = "offline";
        customerplan.status = "in-use";
        customerplan.plan_currency = planobj.plan_pricing.currency;

        return customerplan;
    }


    async getCustomerplanById(id: string): Promise<Customerplan> {

        let res = await axios.get("/customer-plans/"+id, {headers: this.apiHeaders});

        return res.data;
    }




}