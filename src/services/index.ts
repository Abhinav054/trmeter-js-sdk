import * as axios from 'axios';
import { CustomerplanService } from './customerplanService';
import { CustomerService } from './customerservice';

export class TrMeter {

    apitoken:string;

    customerService: CustomerService;

    customerplanService: CustomerplanService;

    constructor(apitoken: string){

        this.apitoken = apitoken;

        this.customerService = new CustomerService(this.apitoken);

        this.customerplanService = new CustomerplanService(this.apitoken);
        
    }


    getToken() {
        return this.apitoken;
    }




}